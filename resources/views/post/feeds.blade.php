<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Blessd.in</title>
    <link type="text/css" rel="stylesheet" href="./html-preview/keeway/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet"
          href="./html-preview/keeway/assets/plugins/bootstrap/css/bootstrap-grid.min.css">
    <link type="text/css" rel="stylesheet"
          href="./html-preview/keeway/assets/plugins/bootstrap/css/bootstrap-reboot.min.css">
    <link type="text/css" rel="stylesheet" href="./html-preview/keeway/assets/css/custom.css">
    <link type="text/css" rel="stylesheet" href="./html-preview/keeway/assets/css/logo_css.css">
    <link type="text/css" rel="stylesheet" href="./html-preview/keeway/assets/css/animate1.css">
    <link type="text/css" rel="stylesheet"
          href="../../../public/html-preview/keeway/assets/fonts/font-awesome/css/font-awesome.min.css">

</head>
<body>

<main>
    <nav class="navbar navbar-toggleable-md navbar-light" style="background: #03a9f4">
        <p class="text-center"><h4 class="text-white">Blessed.in</h4></p>
        <span class="right"><a href="./login"><h5 class="text-white">Login</h5></a></span>
    </nav>
    <div class="py-sm-4 py-4"></div>

    @if(Auth::check() and (Auth::user()->type=='temple' or Auth::user()->type=='preist'))
        <div style="position: relative" class="post-update row col-sm-12 col-12 ">
            <form action="./addPost" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group row col-12 col-sm-12 my-sm-3 my-3">
                    <textarea rows="4" cols="42" placeholder="Share Something !" name="contents" class="ml-sm-5 ml-2"></textarea>
                </div>
                <button type="submit" class="col-sm-4 col-4 ml-sm-3  btn btn-primary float-right float-sm-right">Submit
                </button>
                <input type="file" name="image" class="col-sm-5 col-5 ml-sm-3  btn btn-light float-sm-left float-left">
            </form>
        </div>
    @endif
    <br><br>

    <!--<cards loop here>-->
    <div style="position: relative">
        @php
            $i=0;
        @endphp
        @foreach($posts as $post)
            <div class="card no-padding content">
                <div class="card-body card-body-head">
                    <img src="../../../public/html-preview/keeway/assets/img/4.JPG"
                         class="rounded-circle profile_image mr-sm-1 mr-1">
                    <h6 class="card-title d-inline">{{$name[$i]}}</h6>
                </div>
                @php
                    $i++;
                @endphp
                @if($post->file_src!='')
                    <img class="card-img-top" src="{{$post->file_src}}" alt="Card image">
                @endif

                <div class="card-body py-sm-1 py-1">
                    <p class="card-text">{{$post->content}}</p>
                    <div class="share-buttons">
                        <span href="#"><i class="fa fa-facebook"></i></span>
                        <span><i class="fa fa-whatsapp ml-3 ml-sm-3"></i></span>
                        <span><i class="fa fa-instagram ml-3 ml-sm-3"></i></span>
                        <span><i class="fa fa-twitter ml-3 ml-sm-3"></i></span>
                    </div>
                </div>

                <div class="reactions card-body  my-sm-1 my-1 py-sm-1 py-1">
                    <div class="row">
                        <div class="col-sm-4 col-4  text-left"><p>55K likes</p></div>
                        <div class="col-sm-8 col-8  text-right"><p>5K comments . 2K shares</p></div>
                    </div>
                </div>

            </div>
            <!--</cards loop ends here>-->
        @endforeach
    </div>
</main>


<!--<a href="http://gazpo.com/downloads/tutorials/jquery/scrolltop/#" class="scrollup" style="display: none;"><img src="assets/images/up.png" class="hide-on-small-only up-button right"></a>-->
<script src="./html-preview/keeway/assets/js/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="./html-preview/keeway/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="./html-preview/keeway/assets/js/custom.js"></script>
</body>
</html>
