<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
		<title>Pandit Neeraj Sharma</title>
		<!-- Material icons -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!-- Compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
		
		<!-- Local Stylesheet -->
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body class="red">
		
		<nav>
			<div class="nav-wrapper">
				<a href="#!" class="brand-logo">Blessd.in</a>
				<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a href="sass.html">Sass</a></li>
					<li><a href="badges.html">Components</a></li>
					<li><a href="collapsible.html">Javascript</a></li>
					<li><a href="mobile.html">Mobile</a></li>
				</ul>
			</div>
		</nav>
		<ul class="sidenav" id="mobile-demo">
			<li><a href="sass.html">Sass</a></li>
			<li><a href="badges.html">Components</a></li>
			<li><a href="collapsible.html">Javascript</a></li>
			<li><a href="mobile.html">Mobile</a></li>
		</ul>
		
		<div class="row">
			<div class="col s12 m12 l12 center grey lighten-4" style="padding-bottom: 2em;">
				<br><img src="./images/profile.jpeg" width="200" height="200" alt="" class="circle responsive-img">
				<h4>Pandit Neeraj Sharma</h4>
				<h6>Pandit at Ram Mandir, H Block, Vikaspuri</h6>
				<br>
				<a class="waves-effect waves-light btn amber accent-4">Follow</a>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m12 l12 ">
				<div class="card-panel z-depth-4">
					<h4>About Me</h4>
					<p>Always a keen observer of religious practices, Pt.Neeraj Sharma is a renowned astrologer who has been practicing astrology for many years now. His interest in spirituality developed during his brush with Gemology when he met his present Guruji who discovered the spark in him and advised him to take the path he is on today. He gave it a try and his work was appreciated giving him the encouragement to fuel his passion. </p>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col s12 m12 l12 ">
				<div class="card-panel z-depth-4">
					<h4>Expertise</h4>
					<ul class="browser-default">
						<li class="expertise">Tarot</li>
						<li class="expertise">Astrology</li>
						<li class="expertise">Numerology</li>
						<li class="expertise">Crystal Healing</li>
						<li class="expertise">Reiki</li>
						<li class="expertise">Past-Life Regression</li>
						<li class="expertise">Poojas and Hawans</li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col s12 m12 l12 ">
				<div class="card-panel z-depth-4">
					<h4>Reviews</h4>
					
					<!--<div class="card">
						<div class="card-content ">
							<span class="card-title">
								<a class="waves-effect waves-light btn green">5.0</a>
							</span>
							<p>Always a keen observer of religious practices, Pt.Neeraj Sharma is a renowned astrologer who has been practicing astrology for many years now. </p>
						</div>
					</div>
					<div class="card">
						<div class="card-content ">
							<span class="card-title">
								<a class="waves-effect waves-light btn green">4.8</a>
							</span>
							<p>This is review 2</p>
						</div>
					</div>
					<div class="card">
						<div class="card-content ">
							<span class="card-title">
								<a class="waves-effect waves-light btn green">4.5</a>
							</span>
							<p>This is review 3</p>
						</div>
					</div>-->



					<ul class="collapsible">
						<li>
							<div class="collapsible-header"><a class="waves-effect waves-light btn green">5.0</a> &nbsp; Terrific</div>
							<div class="collapsible-body"><span>Always a keen observer of religious practices, Pt.Neeraj Sharma is a renowned astrologer who has been practicing astrology for many years now.</span></div>
						</li>
						<li>
							<div class="collapsible-header"><a class="waves-effect waves-light btn green">4.8</a> &nbsp; Wow!</div>
							<div class="collapsible-body"><span>This is review 2</span></div>
						</li>
						<li>
							<div class="collapsible-header"><a class="waves-effect waves-light btn green">4.5</a> &nbsp; Very good</div>
							<div class="collapsible-body"><span>This is review 3</span></div>
						</li>
					</ul>
					
				</div>
			</div>
		</div>
		
		<div class="row">
			<form class="col s12">
				<div class="card-panel z-depth-4">
					<div class="row">
						<div class="input-field col s12 center">
							<textarea id="textarea1" class="materialize-textarea"></textarea>
							<label for="textarea1">Write your review here!</label>
							<button class="btn waves-effect waves-light amber accent-4" type="submit" name="action">Submit Review
							<!--<i class="material-icons right">send</i>-->
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		
		<!-- jQuery CDN -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		
		<!-- Compiled and minified JavaScript -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
		
		<!-- Script for collapsible menu -->
		<script>
			$(document).ready(function()
			{
				$('.sidenav').sidenav();
			});
			
			$(document).ready(function()
			{
				$('.collapsible').collapsible();
			});
		</script>
	</body>
</html>